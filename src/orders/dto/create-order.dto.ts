import { IsNotEmpty, IsPositive, Length, MinLength } from 'class-validator';

class CreateOrderItemDto {
  productId: number;
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
}
