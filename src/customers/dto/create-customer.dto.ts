import { IsNotEmpty, IsPositive, Length, MinLength } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  age: number;

  @MinLength(10)
  @IsNotEmpty()
  tel: string;

  @Length(1)
  @IsNotEmpty()
  gender: string;
}
